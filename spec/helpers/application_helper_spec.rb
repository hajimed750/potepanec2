RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title(page_title)" do
    context "page_titleが存在する場合" do
      it "page_title + BASE_TITLE を表示" do
        expect(full_title("Bags")).to eq "Bags - BIGBAG Store"
      end
    end

    context "page_titleがnilの場合" do
      it "BASE_TITLE のみを表示" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end

    context "page_titleがemptyの場合" do
      it "BASE_TITLE のみを表示" do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end
  end
end
