RSpec.describe "Categories_request", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "リクエストが成功すること" do
      expect(response).to be_successful
    end

    it "categories/showがレンダリングされること" do
      expect(response).to render_template :show
    end

    it '@taxonomiesに正しい値が入っていること' do
      expect(response.body).to include taxonomy.name
    end

    it '@taxonに正しい値が入っていること' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@productsに正しい値が入っていること' do
      expect(assigns(:products)).to match_array(product)
    end
  end
end
