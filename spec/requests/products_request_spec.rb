RSpec.describe "Products_request", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "リクエストが成功すること" do
      expect(response).to be_successful
    end

    it "products/showがレンダリングされること" do
      expect(response).to render_template :show
    end

    it "商品データが取得できていること" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
    end

    it "関連商品のデータが取得できていること" do
      related_products.each do |related_product|
        expect(response.body).to include related_product.name
        expect(response.body).to include related_product.display_price.to_s
      end
    end
  end
end
