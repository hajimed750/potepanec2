class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.includes(master: [:images, :default_price]).
      limit(Const::RELATED_PRODUCTS_COUNT).
      related_products(@product)
  end
end
